from flask import Flask
from api import api

def create_app():
    app = Flask(__name__)

    app.register_blueprint(api)

    return app

if __name__ == '__main__':
    app = create_app()
    if app.debug:
        app.run(debug=True)
    else:
        app.run()